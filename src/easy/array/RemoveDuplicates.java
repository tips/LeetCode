package easy.array;

public class RemoveDuplicates {

	/**
	 * 删除重复数字
	 * 
	 * @param nums
	 * @return
	 */
	public static int removeDuplicates(int[] nums) {
		int len = nums.length;
		int i = 0, j = 1;
		for (; j < len; j++) {
			if (nums[i] != nums[j]) {
				if (j - i > 1) {
					nums[i + 1] = nums[j];
				}
				i++;
			}
		}
		return i + 1;
	}

	/**
	 * 加一
	 * 
	 * @param digits
	 * @return
	 */
	public static int[] plusOne(int[] digits) {
		int len = digits.length;
		int carry = 0;
		for (int i = len - 1; i >= 0; i--) {
			if (digits[i] < 9) {
				digits[i] = digits[i] + 1;
				carry = 0;
				break;
			} else {
				digits[i] = 0;
				carry = 1;
			}
		}
		if (carry == 1) {
			int[] expand = new int[len + 1];
			expand[0] = 1;
			return expand;
		}
		return digits;
	}

	/**
	 * 给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。
	 * 
	 * @param nums
	 */
	public static void moveZeroes(int[] nums) {
		int len = nums.length;
		for (int i = 0, j = 1; j < len; j++) {
			if (nums[i] == 0 && nums[j] != 0) {
				nums[i] = nums[j];
				nums[j] = 0;
				i++;
			} else if (nums[i] != 0) {
				i++;
			}
		}
	}

	/**
	 * 数独
	 * 
	 * @param board
	 * @return
	 */
	public static boolean isValidSudoku(char[][] board) {

		int[] rows;
		int[] cols;
		for (int i = 0; i < 9; i++) {
			rows = new int[10];
			cols = new int[10];
			for (int j = 0; j < 9; j++) {
				if (board[i][j] != '.') {
					int index = board[i][j] - '0';
					if (rows[index] == 0) {
						rows[index] = board[i][j];
					} else {
						return false;
					}
				}

				if (board[j][i] != '.') {
					int index = board[j][i] - '0';
					if (cols[index] == 0) {
						cols[index] = board[j][i];
					} else {
						return false;
					}
				}
			}
		}

		int[] nums;
		for (int row = 0; row < 9; row += 3) {
			for (int col = 0; col < 9; col += 3) {
				nums = new int[10];
				for (int i = 0 + row; i < 3 + row; i++) {
					
					for (int j = 0 + col; j < 3 + col; j++) {

						if (board[i][j] != '.') {
							int index = board[i][j] - '0';
							if (nums[index] == 0) {
								nums[index] = board[i][j];
							} else {
								return false;
							}
						}
					}

				}
			}
		}

		return true;
	}

	public static void main(String[] args) {
		char[][] nums = new char[][] {

				{ '5', '3', '.', '.', '7', '.', '.', '.', '.' }, 
				{ '6', '.', '.', '1', '9', '5', '.', '.', '.' },
				{ '.', '9', '8', '.', '.', '.', '.', '6', '.' }, 
				{ '8', '.', '.', '.', '6', '.', '.', '.', '3' },
				{ '4', '.', '.', '8', '.', '3', '.', '.', '1' },
				{ '7', '.', '.', '.', '2', '.', '.', '.', '6' },
				{ '.', '6', '.', '.', '.', '.', '2', '8', '.' },
				{ '.', '.', '.', '4', '1', '9', '.', '.', '5' },
				{ '.', '.', '.', '.', '8', '.', '.', '7', '9' } };
		System.out.println(isValidSudoku(nums));
	}

}
